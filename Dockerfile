FROM openjdk:8-jdk-alpine

######################################################################
# confd installation
######################################################################

RUN mkdir -p /config && \
    apk add --no-cache wget && \
    wget --no-check-certificate -O /usr/local/bin/confd https://github.com/kelseyhightower/confd/releases/download/v0.11.0/confd-0.11.0-linux-amd64 && \
    chmod +x /usr/local/bin/confd

######################################################################
# environment vaiables
######################################################################

ENV EXPLORER_ACTUATOR_USER "zenika"
ENV EXPLORER_ACTUATOR_PASSWORD "zenika"

######################################################################
# import configuration
######################################################################

COPY runtime/confd /etc/confd/
COPY runtime/startup.sh /usr/local/bin/
RUN  chmod +x /usr/local/bin/startup.sh

######################################################################
# import app
######################################################################

COPY target/dict-explorer-*.jar /dict-explorer.jar

######################################################################
# launch confd and springboot
######################################################################

CMD "/usr/local/bin/startup.sh"
