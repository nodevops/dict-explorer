# dict-explorer

[![build status](https://gitlab.com/nodevops/dict-explorer/badges/master/build.svg)](https://gitlab.com/nodevops/dict-explorer/commits/master)

## Examples

To run the application:
```bash
mvn package && \
java -jar target/dict-explorer-1.0-SNAPSHOT.jar
```
This command will print dictionary from the given ordered list.

## Tests

To run the test:
```bash
mvn test
```
Currently we have only unit testing with Junit5.
