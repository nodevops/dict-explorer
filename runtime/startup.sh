#!/bin/ash

if /usr/local/bin/confd -onetime -backend env; then
	echo
    echo "w00t w00t!! config has been generated, let's start our springboot app now"
    java -Djava.security.egd=file:/dev/./urandom -jar /dict-explorer.jar --spring.profiles.active=runtime
else
	echo "You failed at starting confd and/or springboot properly"
fi
