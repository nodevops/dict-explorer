package zenika.nodevops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DictExplorerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DictExplorerApplication.class, args);
    }
}
