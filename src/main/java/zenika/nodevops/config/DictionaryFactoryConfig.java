package zenika.nodevops.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import zenika.nodevops.model.DictionaryFactory;
import zenika.nodevops.model.RunTimeDictionaryFactory;

/**
 * Created by jules on 19/06/17.
 *
 * Allow external configuration for the factory of dictionaries
 */
@Configuration
public class DictionaryFactoryConfig {

    @Bean
    public DictionaryFactory dictionaryFactory() {
        return new RunTimeDictionaryFactory(
                "#",
                "="
        );
    }
}
