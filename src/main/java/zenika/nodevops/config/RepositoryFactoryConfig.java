package zenika.nodevops.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import zenika.nodevops.model.RepositoryFactory;
import zenika.nodevops.model.RunTimeRepositoryFactory;

/**
 * Created by jules on 20/06/17.
 *
 * Allows external configuration for the factory of repository
 */
@Configuration
public class RepositoryFactoryConfig {

    @Bean
    public RepositoryFactory repositoryFactory() {
        return new RunTimeRepositoryFactory("target/repositories/", 1);
    }
}
