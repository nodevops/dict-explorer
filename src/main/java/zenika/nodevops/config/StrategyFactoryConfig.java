package zenika.nodevops.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import zenika.nodevops.model.RunTimeStrategyFactory;
import zenika.nodevops.model.StrategyFactory;

/**
 * Created by jules on 21/06/17.
 */
@Configuration
public class StrategyFactoryConfig {

    @Bean
    public StrategyFactory strategyFactory() {
        return new RunTimeStrategyFactory();
    }

}
