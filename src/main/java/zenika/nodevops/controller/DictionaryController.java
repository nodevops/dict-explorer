package zenika.nodevops.controller;

import org.eclipse.jgit.api.Git;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import zenika.nodevops.model.BiDictionaryRequest;
import zenika.nodevops.model.Dictionary;
import zenika.nodevops.model.DictionaryFactory;
import zenika.nodevops.model.DictionaryRequest;
import zenika.nodevops.model.DifferenceBetweenDictionaries;
import zenika.nodevops.model.EnvironmentNotFoundException;
import zenika.nodevops.model.RepositoryCanNotBeCreatedException;
import zenika.nodevops.model.RepositoryFactory;
import zenika.nodevops.model.Strategy;
import zenika.nodevops.model.StrategyCanNotBeAppliedException;
import zenika.nodevops.model.StrategyFactory;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by jules on 12/06/17.
 *
 * Explore feature linked to Dictionary
 */
@RestController
@RequestMapping("/api/v1")
@CrossOrigin
public class DictionaryController {

    @Autowired
    private DictionaryFactory dictionaryFactory;

    @Autowired
    private RepositoryFactory repositoryFactory;

    @Autowired
    private StrategyFactory strategyFactory;

    @PostMapping(
            path = "/dict",
            headers = "content-type=application/json")
    public Dictionary getDictionary(@RequestBody DictionaryRequest dictionaryRequest) throws RuntimeException {
        return createDictionary(dictionaryRequest);
    }

    @PostMapping(
            path = "/diff",
            headers = "content-type=application/json")
    public DifferenceBetweenDictionaries diffDictionary(@RequestBody BiDictionaryRequest biDictionaryRequest) throws RuntimeException {
        Dictionary dictLeft = createDictionary(splitLeft(biDictionaryRequest));
        Dictionary dictRight = createDictionary(splitRight(biDictionaryRequest));
        return diff(dictLeft, dictRight);
    }

    private static DifferenceBetweenDictionaries diff(Dictionary dictLeft, Dictionary dictRight) {
        DifferenceBetweenDictionaries difference = new DifferenceBetweenDictionaries();
        HashSet<String> variablesNameSet = new HashSet<>();
        variablesNameSet.addAll(dictLeft.extractVariableNames());
        variablesNameSet.addAll(dictRight.extractVariableNames());
        variablesNameSet.forEach((String varName) -> difference.add(varName, dictLeft, dictRight));
        return difference;
    }

    private Dictionary createDictionary(DictionaryRequest dictionaryRequest) {
        Git git = repositoryFactory.make(dictionaryRequest.getUrl());
        Strategy strategy = strategyFactory.make();
        ArrayList<String> filesList = strategy.apply(
                git,
                dictionaryRequest.getEnvName(),
                dictionaryRequest.getVersionNumber()
        );
        Dictionary dictionary = dictionaryFactory.make(
                git.getRepository().getDirectory().getParent(),
                filesList,
                dictionaryRequest.getEnvName(),
                dictionaryRequest.getVersionNumber()
        );
        repositoryFactory.delete(git);
        return dictionary;
    }

    private DictionaryRequest splitLeft(BiDictionaryRequest biDictionaryRequest) {
        return new DictionaryRequest(
                biDictionaryRequest.getUrl(),
                biDictionaryRequest.getEnvName(),
                biDictionaryRequest.getVersionNumber());
    }

    private DictionaryRequest splitRight(BiDictionaryRequest biDictionaryRequest) {
        return new DictionaryRequest(
                biDictionaryRequest.getUrlSecond(),
                biDictionaryRequest.getEnvNameSecond(),
                biDictionaryRequest.getVersionNumberSecond());
    }

    @ExceptionHandler(RepositoryCanNotBeCreatedException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String repositoryClonedError(RepositoryCanNotBeCreatedException e) {
        return e.getMessage();
    }

    @ExceptionHandler(EnvironmentNotFoundException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String checkoutError(EnvironmentNotFoundException e) {
        return e.getMessage();
    }

    @ExceptionHandler(StrategyCanNotBeAppliedException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String startegyAppliedError(StrategyCanNotBeAppliedException e) {
        return e.getMessage();
    }
}
