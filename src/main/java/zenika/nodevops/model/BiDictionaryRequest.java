package zenika.nodevops.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by jules on 07/07/17.
 *
 * Represent data receive when user wants 2 dictionary
 */
public class BiDictionaryRequest extends DictionaryRequest {

    @JsonProperty
    private String urlSecond;
    @JsonProperty
    private String envNameSecond;
    @JsonProperty
    private String versionNumberSecond;

    public BiDictionaryRequest(
            String url,
            String envName,
            String versionNumber,
            String urlSecond,
            String envNameSecond,
            String versionNumberSecond) {
        super(url, envName, versionNumber);
        this.urlSecond = urlSecond;
        this.envNameSecond = envNameSecond;
        this.versionNumberSecond = versionNumberSecond;
    }

    public BiDictionaryRequest() {
    }

    public String getUrlSecond() {
        return urlSecond;
    }

    public void setUrlSecond(String urlSecond) {
        this.urlSecond = urlSecond;
    }

    public String getEnvNameSecond() {
        return envNameSecond;
    }

    public void setEnvNameSecond(String envNameSecond) {
        this.envNameSecond = envNameSecond;
    }

    public String getVersionNumberSecond() {
        return versionNumberSecond;
    }

    public void setVersionNumberSecond(String versionNumberSecond) {
        this.versionNumberSecond = versionNumberSecond;
    }
}
