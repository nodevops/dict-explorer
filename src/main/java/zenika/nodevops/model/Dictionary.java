package zenika.nodevops.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by jules on 24/05/17.
 *
 * Represent an set of variables with the history of values for its variables
 * Specific for a environment and a version
 */
public class Dictionary {

    public  static final String UNDEFINED = "undefined";

    @JsonProperty
    private String envName = UNDEFINED;
    @JsonProperty
    private String version = UNDEFINED;
    @JsonProperty
    private HashSet<Variable> variables = null;

    Dictionary(String name, String version) {
        this.envName = name;
        this.version = version;
        this.variables = new HashSet<>();
    }

    public Dictionary() {
    }

    void add(String key, String value, String origin, int lineNumber) {
        VariableHistory varHistory = ImmutableVariableHistory.builder()
                .origin(origin)
                .value(value)
                .lineNumber(lineNumber)
                .build();
        addVariableHistory(key, varHistory);
    }

    private void addVariableHistory(String key, VariableHistory variableHistory) {
        Optional<Variable> optVar = getVariableByName(key);
        Variable var;
        if (optVar.isPresent()) {
            var = optVar.get();
        } else {
            var = new Variable(key);
            this.variables.add(var);
        }
        var.addStepHistory(variableHistory);
    }

    public Optional<Variable> getVariableByName(String variableName) {
        for (Variable variable : this.variables) {
            if (variable.getName().equals(variableName)) {
                return Optional.of(variable);
            }
        }
        return Optional.empty();
    }

    @JsonIgnore
    public String getFullName() {
        return this.envName + "_" + this.version;
    }

    public Collection<String> extractVariableNames() {
        return this.variables.stream()
                .map(Variable::getName)
                .collect(Collectors.toList());
    }

    public HashSet<Variable> getVariables() {
        return variables;
    }
}
