package zenika.nodevops.model;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by jules on 19/06/17.
 *
 * Interface for dictionaries' factories
 */
public interface DictionaryFactory {

    Dictionary make(String pathRoot, ArrayList<String> fileList, String env, String version);

    Map getWarnings();
}
