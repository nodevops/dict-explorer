package zenika.nodevops.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by jules on 07/07/17.
 *
 * Represent data receive when user wants 1 dictionary
 */
public class DictionaryRequest {

    @JsonProperty
    private String url;
    @JsonProperty
    private String envName;
    @JsonProperty
    private String versionNumber;

    public DictionaryRequest(String url, String envName, String versionNumber) {
        this.url = url;
        this.envName = envName;
        this.versionNumber = versionNumber;
    }

    public DictionaryRequest() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEnvName() {
        return envName;
    }

    public void setEnvName(String envName) {
        this.envName = envName;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }
}
