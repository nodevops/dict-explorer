package zenika.nodevops.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Optional;

/**
 * Created by jules on 14/06/17.
 *
 * Represent difference between
 */
public class DifferenceBetweenDictionaries extends HashMap<String, LinkedList<DifferenceInConfigurationValues>> {

    public DifferenceBetweenDictionaries() {
        this.put(Where.BOTH_SAME.toString(), new LinkedList<>());
        this.put(Where.BOTH_DIFF.toString(), new LinkedList<>());
        this.put(Where.ONLY_LEFT.toString(), new LinkedList<>());
        this.put(Where.ONLY_RIGHT.toString(), new LinkedList<>());
        this.put(Where.ERROR.toString(), new LinkedList<>());
    }

    public void add(String varName, Dictionary dictLeft, Dictionary dictRight) {
        Where category;
        String currentValue;
        Variable varLeft;
        Variable varRight;
        Optional<Variable> optLeftVar = dictLeft.getVariableByName(varName);
        Optional<Variable> optRightVar = dictRight.getVariableByName(varName);
        if ((optLeftVar.isPresent()) && (optRightVar.isPresent())) {
            varLeft = optLeftVar.get();
            varRight = optRightVar.get();
            if (optLeftVar.get().getValue().equals(optRightVar.get().getValue())) {
                category = Where.BOTH_SAME;
                currentValue = varLeft.getValue();
            } else {
                category = Where.BOTH_DIFF;
                currentValue = null;
            }
        } else if (optLeftVar.isPresent()) {
            category = Where.ONLY_LEFT;
            varLeft = optLeftVar.get();
            varRight = null;
            currentValue = varLeft.getValue();
        } else if (optRightVar.isPresent()) {
            category = Where.ONLY_RIGHT;
            varLeft = null;
            varRight = optRightVar.get();
            currentValue = varRight.getValue();
        } else {
            category = Where.ERROR;
            varLeft = null;
            varRight = null;
            currentValue = null;
        }
        this.get(category.toString()).add(new DifferenceInConfigurationValues(varName, varLeft, varRight, currentValue));
    }
}
