package zenika.nodevops.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by jules on 14/06/17.
 *
 * Class of the key compared with history
 */
public class DifferenceInConfigurationValues {
    @JsonProperty
    private String key;
    @JsonProperty
    private Variable left;
    @JsonProperty
    private Variable right;
    @JsonProperty
    private String currentValue;

    public DifferenceInConfigurationValues(String key, Variable left, Variable right, String value) {
        this.key = key;
        this.currentValue = value;
        this.left = left;
        this.right = right;
    }

    String getKey() {
        return key;
    }

    String getCurrentValue() {
        return currentValue;
    }

    Variable getLeft() {
        return left;
    }

    Variable getRight() {
        return right;
    }
}
