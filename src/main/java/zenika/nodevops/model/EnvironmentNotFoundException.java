package zenika.nodevops.model;

/**
 * Created by jules on 11/07/17.
 *
 * Raises when the tag is wrong
 */
public class EnvironmentNotFoundException extends RuntimeException {

    EnvironmentNotFoundException(String message) {
        super(message);
    }
}
