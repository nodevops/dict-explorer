package zenika.nodevops.model;

/**
 * Created by jules on 11/07/17.
 *
 * Raises when the repository cloning occurs with error
 */
public class RepositoryCanNotBeCreatedException extends RuntimeException {
    public RepositoryCanNotBeCreatedException(String message) {
        super(message);
    }
}
