package zenika.nodevops.model;

import org.eclipse.jgit.api.Git;

/**
 * Created by jules on 20/06/17.
 *
 * Factory to clone a repository from the url
 */
public interface RepositoryFactory {

    Git make(String url) throws RuntimeException;

    void delete(Git git);
}
