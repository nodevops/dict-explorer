package zenika.nodevops.model;

import org.springframework.util.Assert;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jules on 24/05/17.
 *
 * Allow the creation of dictionary where value are split and no-commented
 */
public class RunTimeDictionaryFactory implements DictionaryFactory {

    private static final Charset DEFAULT_CHARSET = Charset.forName("US-ASCII");
    private String commentStart = "//";
    private String splitChar = "=";
    private Map<String, String> warnings = new HashMap<>();

    public RunTimeDictionaryFactory(String com, String split) {
        this.commentStart = com;
        this.splitChar = split;
    }

    /**
     * Create new Dictionary by indicate environment and version and the ordered file list
     *
     * @param pathRoot : the root of all files contain in the list
     * @param fileList : the file list, just the name of files
     * @param env      :      the environment which is built the dictionary
     * @param version  :  the version which is built the dictionary
     * @return the produced dictionary
     */
    @Override public Dictionary make(String pathRoot, ArrayList<String> fileList, String env, String version) {
        Dictionary dictionary = new Dictionary(env, version);

        Assert.notNull(pathRoot, "Path root for create dictionary must be not null!");
        Assert.notNull(fileList, "Files list for create dictionary must be not null!");

        for (String fileName : fileList) {
            parseFile(dictionary, pathRoot, fileName);
        }
        return dictionary;
    }

    private void parseFile(Dictionary dictionary, String pathRoot, String filename) {
        String fullPathFile = createFullPathFile(pathRoot, filename);
        File dictionaryFile = new File(fullPathFile);
        if (isFileValid(dictionaryFile)) {
            try (BufferedReader reader = Files.newBufferedReader(dictionaryFile.toPath(), DEFAULT_CHARSET)) {
                String line;
                int lineNumber = 1;
                while ((line = reader.readLine()) != null) {
                    if (!line.startsWith(this.commentStart)) {
                        if (line.contains(this.splitChar)) {
                            String[] variableValue = line.split(this.splitChar);
                            dictionary.add(variableValue[0], variableValue[1], filename, lineNumber);
                        } else {
                            warnings.put(
                                    filename + ":" + lineNumber,
                                    "no split char " + splitChar + " in line : " + line);
                        }
                    }
                    lineNumber++;
                }
                reader.close();
            } catch (IOException x) {
                System.err.format("IOException: %s%n", x);
            }
        }
    }

    String createFullPathFile(String pathRoot, String fileName) {
        if (fileName.startsWith("/")) {
            throw new IllegalArgumentException(
                    "Second parameter: file must be relative to the first parameter!"
            );
        } else {
            if (pathRoot.endsWith("/")) {
                return pathRoot + fileName;
            } else {
                return pathRoot + "/" + fileName;
            }
        }
    }

    boolean isFileValid(File dictionaryFile) {
        return dictionaryFile.exists() && dictionaryFile.isFile() && dictionaryFile.canRead();
    }

    @Override public Map getWarnings() {
        return Collections.unmodifiableMap(new HashMap<>(warnings));
    }

}
