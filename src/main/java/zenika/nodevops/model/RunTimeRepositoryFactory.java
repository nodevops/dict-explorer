package zenika.nodevops.model;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.springframework.util.Assert;

import java.io.File;
import java.util.Random;

/**
 * Created by jules on 20/06/17.
 *
 * Mock the process to test from local url
 */
public class RunTimeRepositoryFactory implements RepositoryFactory {

    private File workDirectory;
    private Random random;

    public RunTimeRepositoryFactory(String workDir, long seed) {
        this.workDirectory = new File(workDir);
        cleanDirectory(this.workDirectory);
        this.workDirectory.mkdir();
        this.random = new Random();
        this.random.setSeed(seed);
    }

    @Override
    public Git make(String url) throws RuntimeException {

        Assert.notNull(url, "URL to clone the repository must not be empty!");

        Git git = null;
        String path = workDirectory.getAbsolutePath() + "/" + getNextRepositoryId();
        try {
            git = Git.cloneRepository()
                    .setURI(url)
                    .setDirectory(new File(path))
                    .call();
        } catch (GitAPIException e) {
            throw new RepositoryCanNotBeCreatedException("repo from "+url+" couldn't be created!");
        }

        return git;
    }

    private boolean cleanDirectory(File workDir) {
        if (workDir.isDirectory()) {
            File[] files = workDir.listFiles();
            if (files != null && files.length > 0) {
                for (File file : files) {
                    cleanDirectory(file);
                }
            }
        }
        return workDir.delete();
    }

    @Override
    public void delete(Git git) {
        File dotGit = git.getRepository().getDirectory();
        String pathRoot = dotGit.getParent();
        cleanDirectory(new File(pathRoot));
    }

    int getNextRepositoryId() {
        return Math.abs(this.random.nextInt());
    }
}
