package zenika.nodevops.model;

/**
 * Created by jules on 21/06/17.
 */
public class RunTimeStrategyFactory implements StrategyFactory {

    @Override public Strategy make() {
        return new Strategy();
    }
}
