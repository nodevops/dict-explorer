package zenika.nodevops.model;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by jules on 20/06/17.
 *
 * A Strategy implements the way to create a dictionary from a repository by extracting a file list
 */
public class Strategy {

    public ArrayList<String> apply(Git git, String envTargeted, String numberTargeted) throws RuntimeException {
        checkout(git, getTag(envTargeted, numberTargeted));
        ArrayList<String> filesList = createFileNameListOrdered(envTargeted);
        if (nbFileFound(git, filesList) == 0) {
            throw new StrategyCanNotBeAppliedException("None of theses files found from this file list:" + filesList);
        }
        return filesList;
    }

    private void checkout(Git git, String tagTargeted) throws EnvironmentNotFoundException {
        try {
            git.checkout().setName(tagTargeted).call();
        } catch (GitAPIException e) {
            throw new EnvironmentNotFoundException("Checkout impossible for tag:" + tagTargeted);
        }
    }

    ArrayList<String> createFileNameListOrdered(String envTargeted) {
        ArrayList<String> list = new ArrayList<>();
        String pathDictionaries = "dictionaries/";
        list.add(pathDictionaries + "common.dict");
        list.add(pathDictionaries + envTargeted + "/main.dict");
        return list;
    }

    String getTag(String envTargeted, String numberTargeted) {
        return "v" + numberTargeted + "_" + envTargeted;
    }

    private int nbFileFound(Git git, ArrayList<String> filesList) {
        int count = 0;
        String absoluteRootPath = git.getRepository().getDirectory().getParent();
        for (String fileName : filesList) {
            File file = new File(absoluteRootPath + "/" + fileName);
            if (file.isFile() && file.exists() && file.canRead()) {
                count++;
            }
        }
        return count;
    }
}
