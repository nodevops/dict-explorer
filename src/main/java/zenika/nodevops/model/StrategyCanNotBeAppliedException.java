package zenika.nodevops.model;

/**
 * Created by jules on 11/07/17.
 *
 * Raises when none of files are found
 */
public class StrategyCanNotBeAppliedException extends RuntimeException{

    public StrategyCanNotBeAppliedException(String message) {
        super(message);
    }
}
