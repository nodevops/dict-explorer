package zenika.nodevops.model;

/**
 * Created by jules on 21/06/17.
 */
public interface StrategyFactory {
    Strategy make();
}
