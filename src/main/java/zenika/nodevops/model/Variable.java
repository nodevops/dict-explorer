package zenika.nodevops.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.LinkedList;

/**
 * Created by jules on 24/05/17.
 *
 * Represent the variable: its name, its historic
 */
public class Variable {
    static final String UNDEFINED = "undefined";

    @JsonProperty
    private String name = UNDEFINED;
    @JsonProperty
    private String value = UNDEFINED;
    @JsonProperty
    private String origin = UNDEFINED;
    @JsonProperty
    private LinkedList<VariableHistory> valuesHistory = null;

    Variable(String name) {
        this.name = name;
        this.valuesHistory = new LinkedList<>();
    }

    public Variable() {
    }

    void addStepHistory(VariableHistory step) {
        this.value = step.value();
        this.origin = step.origin();
        this.valuesHistory.addLast(step);
    }

    LinkedList<VariableHistory> getValuesHistory() {
        return valuesHistory;
    }

    public String toString() {
        return this.name + " = " + this.value;
    }

    String getName() {
        return name;
    }

    String getValue() {
        return value;
    }

    String getOrigin() {
        return origin;
    }
}
