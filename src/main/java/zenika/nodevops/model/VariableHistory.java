package zenika.nodevops.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * Created by jules on 24/05/17.
 *
 * Represent a couple of a value in a specific file
 */
@Value.Immutable
@JsonSerialize(as = ImmutableVariableHistory.class)
@JsonDeserialize(as = ImmutableVariableHistory.class)
public abstract class VariableHistory {
    @JsonProperty public abstract String value();

    @JsonProperty public abstract String origin();

    @JsonProperty public abstract int lineNumber();
}
