package zenika.nodevops.model;

/**
 * Created by jules on 14/06/17.
 *
 * Represent the type of difference for two variables
 */
public enum Where {
    BOTH_SAME("variables_available_on_both_and_same_value"),
    BOTH_DIFF("variables_available_on_both_but_different_value"),
    ONLY_LEFT("variables_only_available_in_left"),
    ONLY_RIGHT("variables_only_available_in_right"),
    ERROR("ERROR");

    private String message;

    Where(String where) {
        this.message = where;
    }

    @Override public String toString() {
        return message;
    }
}
