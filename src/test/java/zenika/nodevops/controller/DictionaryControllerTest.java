package zenika.nodevops.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import zenika.nodevops.model.BiDictionaryRequest;
import zenika.nodevops.model.DictionaryFactory;
import zenika.nodevops.model.DictionaryRequest;
import zenika.nodevops.model.RepositoryFactory;
import zenika.nodevops.model.StrategyFactory;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by jules on 04/07/17.
 *
 * Test the rest controller API
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DictionaryControllerTest {

    private final String DICT_URL = "/api/v1/dict";
    private final String DIFF_URL = "/api/v1/diff";

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private DictionaryFactory dictionaryFactory;
    @Autowired
    private RepositoryFactory repositoryFactory;
    @Autowired
    private StrategyFactory strategyFactory;
    @Autowired
    private ObjectMapper mapper;

    private DictionaryRequest dictionaryRequest;
    private BiDictionaryRequest biDictionaryRequest;

    @Before
    public void setUp() {
        dictionaryRequest = new DictionaryRequest(
                "https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git",
                "dev-local",
                "0.2.4"
        );
        biDictionaryRequest = new BiDictionaryRequest(
                "https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git",
                "dev-local",
                "0.2.4",
                "https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git",
                "prod",
                "0.2.4"
        );
    }

    @Test
    public void shouldReturnDictionary() throws Exception {
        final int variablesLength = 20;
        this.mockMvc
                .perform(post(DICT_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.mapper.writeValueAsString(this.dictionaryRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.envName", is("dev-local")))
                .andExpect(jsonPath("$.version", is("0.2.4")))
                .andExpect(jsonPath("$.variables", hasSize(variablesLength)))
                .andExpect(jsonPath("$.variables[*].name", notNullValue()))
                .andExpect(jsonPath("$.variables[*].value", notNullValue()));
    }

    @Test
    public void shouldFailForNoExistingEnvironmentDictionary() throws Exception {
        this.dictionaryRequest.setEnvName("zenika");
        this.mockMvc
                .perform(post(DICT_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.mapper.writeValueAsString(this.dictionaryRequest)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldFailForNoRepositoryAtURLDictionary() throws Exception {
        this.dictionaryRequest.setUrl("https://gitlab.com/zenika/test.git");
        this.mockMvc
                .perform(post(DICT_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.mapper.writeValueAsString(this.dictionaryRequest)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldFailForNoExistingVersionDictionary() throws Exception {
        this.dictionaryRequest.setVersionNumber("0.0.0");
        this.mockMvc
                .perform(post(DICT_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.mapper.writeValueAsString(this.dictionaryRequest)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturnDifference() throws Exception {
        final int variablesAvailableOnBothButDifferentValueLength = 4;
        final int variablesAvailableOnBothAndSameValueLength = 16;
        this.mockMvc
                .perform(post(DIFF_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.mapper.writeValueAsString(this.biDictionaryRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.ERROR.length()", is(0)))
                .andExpect(jsonPath("$.variables_only_available_in_right.length()", is(0)))
                .andExpect(jsonPath("$.variables_only_available_in_right.length()", is(0)))
                .andExpect(jsonPath(
                        "$.variables_available_on_both_but_different_value.length()",
                        is(variablesAvailableOnBothButDifferentValueLength)))
                .andExpect(jsonPath(
                        "$.variables_available_on_both_and_same_value.length()",
                        is(variablesAvailableOnBothAndSameValueLength)));
    }

    @Test
    public void shouldFailForNoExistingVersionLeftDifference() throws Exception {
        this.biDictionaryRequest.setVersionNumber("0.0.0");
        this.mockMvc
                .perform(post(DIFF_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.mapper.writeValueAsString(this.biDictionaryRequest)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldFailForNoExistingVersionRightDifference() throws Exception {
        this.biDictionaryRequest.setVersionNumberSecond("0.0.0");
        this.mockMvc
                .perform(post(DIFF_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.mapper.writeValueAsString(this.biDictionaryRequest)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldFailForNoExistingEnvironmentLeftDifference() throws Exception {
        this.biDictionaryRequest.setEnvName("zenika");
        this.mockMvc
                .perform(post(DIFF_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.mapper.writeValueAsString(this.biDictionaryRequest)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldFailForNoExistingEnvironmentRightDifference() throws Exception {
        this.biDictionaryRequest.setEnvNameSecond("zenika");
        this.mockMvc
                .perform(post(DIFF_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.mapper.writeValueAsString(this.biDictionaryRequest)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldFailForNoRepositoryAtURLLeftDifference() throws Exception {
        this.biDictionaryRequest.setUrl("https://gitlab.com/zenika/test.git");
        this.mockMvc
                .perform(post(DIFF_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.mapper.writeValueAsString(this.biDictionaryRequest)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldFailForNoRepositoryAtURLRightDifference() throws Exception {
        this.biDictionaryRequest.setUrlSecond("https://gitlab.com/zenika/test.git");
        this.mockMvc
                .perform(post(DIFF_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.mapper.writeValueAsString(this.biDictionaryRequest)))
                .andExpect(status().isBadRequest());
    }
}
