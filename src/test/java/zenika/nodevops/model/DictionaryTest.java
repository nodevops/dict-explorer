package zenika.nodevops.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by jules on 13/06/17.
 *
 * Test Dictionary object
 */
@RunWith(SpringRunner.class)
@JsonTest
public class DictionaryTest {

    private final int NBADDING = 3;

    @Autowired
    private JacksonTester<Dictionary> json;

    private Dictionary dictionary;

    @Before
    public void setUp() {
        this.dictionary = new Dictionary(
                "test",
                "test"
        );
    }

    @Test
    public void testBuilder() {
        Assert.assertNotNull(
                "Initialization failed",
                new Dictionary(
                        "test",
                        "test"
                )
        );
    }

    @Test
    public void testGetFullName() {
        Assert.assertEquals(
                "Wrong full name!",
                "test" + "_" + "test",
                this.dictionary.getFullName()
        );
    }

    @Test
    public void testAdd() {
        dictionary.add("test", "value", "test", 1);
        dictionary.add("test1", "value", "test", 1);
        dictionary.add("test2", "value", "test", 1);
        Assert.assertEquals(
                "Wrong adding",
                NBADDING,
                dictionary.getVariables().size()
        );
    }

    @Test
    public void testGetVariableByName() {
        dictionary.add("test", "value", "test", 1);
        Assert.assertTrue(
                "Variable not found",
                dictionary.getVariableByName("test").isPresent()
        );
    }

    @Test
    public void testExtractVariableNames() {
        dictionary.add("test", "value", "test", 1);
        dictionary.extractVariableNames().forEach(
                (String key) -> Assert.assertEquals(
                        "Key incorrect",
                        "test",
                        key
                )
        );
    }

    @Test
    public void testSerialize() throws IOException {
        JsonContent<Dictionary> contentJson = this.json.write(dictionary);
        assertThat(contentJson).isEqualToJson("DictionaryExpected.json");
    }

    @Test
    public void testDeserialize() throws IOException {
        Dictionary dictionary = this.json.readObject("DictionaryExpected.json");
        Assert.assertEquals(
                "Deserialization failed!",
                "test" + "_" + "test",
                dictionary.getFullName()
        );
    }
}
