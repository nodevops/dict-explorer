package zenika.nodevops.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by jules on 06/07/17.
 */
public class DifferenceBetweenDictionariesTest {

    private DifferenceBetweenDictionaries differenceBetweenDictionaries;

    @Before
    public void setUp() {
        this.differenceBetweenDictionaries = new DifferenceBetweenDictionaries();
    }

    @Test
    public void testBuilder() {
        Assert.assertNotNull(
                "Wrong initialization!",
                new DifferenceBetweenDictionaries()
        );
    }

    @Test
    public void testInitializationEmptyBothSame() {
        for (String key :
                this.differenceBetweenDictionaries.keySet()) {
            Assert.assertEquals(
                    "Error List " + key,
                    0,
                    this.differenceBetweenDictionaries.get(key).size()
            );
        }
    }
}
