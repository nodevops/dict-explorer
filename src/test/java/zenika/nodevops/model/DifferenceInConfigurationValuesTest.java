package zenika.nodevops.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by jules on 06/07/17.
 */
public class DifferenceInConfigurationValuesTest {

    private DifferenceInConfigurationValues differenceInConfigurationValues;

    @Before
    public void setUp() {
        this.differenceInConfigurationValues = new DifferenceInConfigurationValues(
                "testKey",
                new Variable("testLeft"),
                new Variable("testRight"),
                "testValue"
        );
    }

    @Test
    public void testGetKey() {
        Assert.assertEquals(
                "GetKey doesn't work!",
                "testKey",
                this.differenceInConfigurationValues.getKey()
        );
    }

    @Test
    public void testGetCurrentValue() {
        Assert.assertEquals(
                "GetCurrentValue doesn't work!",
                "testValue",
                this.differenceInConfigurationValues.getCurrentValue()
        );
    }

    @Test
    public void testGetLeft() {
        Assert.assertEquals(
                "GetLeft doesn't work!",
                "testLeft",
                this.differenceInConfigurationValues.getLeft().getName()
        );
    }

    @Test
    public void testGetRight() {
        Assert.assertEquals(
                "GetRight doesn't work!",
                "testRight",
                this.differenceInConfigurationValues.getRight().getName()
        );
    }
}
