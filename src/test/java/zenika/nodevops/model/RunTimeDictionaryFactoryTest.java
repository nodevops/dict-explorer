package zenika.nodevops.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by jules on 13/06/17.
 *
 * Test if the dictionary factory works
 */
public class RunTimeDictionaryFactoryTest {

    private RunTimeDictionaryFactory runTimeDictionaryFactory;

    @Before
    public void setUp() {
        this.runTimeDictionaryFactory = new RunTimeDictionaryFactory("#", "=");
    }

    @Test
    public void testBuilder() {
        Assert.assertNotNull(
                "Wrong initialisation",
                new RunTimeDictionaryFactory("#", "=")
        );
    }

    @Test
    public void testMakeDictionary() {
        ArrayList<String> filesList = new ArrayList<>();
        filesList.add("common.dict");
        Assert.assertNotNull(
                "No dictionary made",
                this.runTimeDictionaryFactory.make(
                        "src/test/example/dictionaries/",
                        filesList,
                        "test",
                        "test"
                )
        );
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMakeDictionaryNullParameters() {
        this.runTimeDictionaryFactory.make(null, new ArrayList<>(), "", "");
        this.runTimeDictionaryFactory.make("", null, "", "");
    }

    @Test
    public void testCreateFullPathFile() {
        String path;
        String filename;

        path = "src/test/example/dictionaries/";
        filename = "test.dict";
        Assert.assertEquals(
                "message",
                path + filename,
                this.runTimeDictionaryFactory.createFullPathFile(path, filename)
        );

        path = "src/test/example/dictionaries";
        filename = "test.dict";
        Assert.assertEquals(
                "message",
                path + "/" + filename,
                this.runTimeDictionaryFactory.createFullPathFile(path, filename)
        );
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateFullPathFileErrorParameters() {
        String path = "src/test/example/dictionaries";
        String filename = "/test.dict";
        this.runTimeDictionaryFactory.createFullPathFile(path, filename);
    }

    @Test
    public void testIsFileValid() {
        String filePath;
        File dictionaryFile;

        filePath = "src/test/example/dictionaries/common.dict";
        dictionaryFile = new File(filePath);
        Assert.assertTrue(
                "Error with file:" + filePath,
                this.runTimeDictionaryFactory.isFileValid(dictionaryFile)
        );

        filePath = "src/test/example/dictionaries/error.dict";
        dictionaryFile = new File(filePath);
        Assert.assertFalse(
                "Error with file:" + filePath,
                this.runTimeDictionaryFactory.isFileValid(dictionaryFile)
        );
    }
}
