package zenika.nodevops.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by jules on 06/07/17.
 *
 * Test if the Repository Factory works well
 */
public class RunTimeRepositoryFactoryTest {

    private RunTimeRepositoryFactory runTimeRepositoryFactory;

    @Before
    public void steUp() {
        this.runTimeRepositoryFactory = new RunTimeRepositoryFactory(
                "target/repositories/",
                1
        );
    }

    @Test
    public void testBuilder() {
        Assert.assertNotNull(
                "Wrong initialization!",
                new RunTimeRepositoryFactory("target/repositories/", 0)
        );
    }

    @Test
    public void testGetNextRepositoryId() {
        Assert.assertNotEquals(
                "Random doesn't work!",
                this.runTimeRepositoryFactory.getNextRepositoryId(),
                this.runTimeRepositoryFactory.getNextRepositoryId()
        );
    }
}
