package zenika.nodevops.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by jules on 06/07/17.
 *
 * Test if the Strategy Factory works well
 */
public class RunTimeStrategyFactoryTest {

    private RunTimeStrategyFactory runTimeStrategyFactory;

    @Before
    public void setUp() {
        this.runTimeStrategyFactory = new RunTimeStrategyFactory();
    }

    @Test
    public void testBuilder() {
        Assert.assertNotNull(
                "Wrong initialisation",
                new RunTimeStrategyFactory()
        );
    }

    @Test
    public void testMake() {
        Assert.assertNotNull(
                "Make Strategy error",
                this.runTimeStrategyFactory.make()
        );
    }
}
