package zenika.nodevops.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by jules on 22/06/17.
 *
 * Test if the default strategy works
 */
public class StrategyTest {

    private Strategy strategy;

    @Before
    public void setUp() {
        this.strategy = new Strategy();
    }

    @Test
    public void testBuilder() {
        Assert.assertNotNull(
                "Initialization failed!",
                new Strategy()
        );
    }

    @Test
    public void testCreateFileNameListOrdered() {
        ArrayList<String> expected = new ArrayList<>();
        expected.add("dictionaries/common.dict");
        expected.add("dictionaries/test/main.dict");
        Assert.assertEquals(
                "Wrong appliance of the strategy!",
                expected,
                this.strategy.createFileNameListOrdered("test")
        );
    }

    @Test
    public void testGetTag() {
        Assert.assertEquals(
                "Tag built wrong!",
                "v" + "0.0.0" + "_" + "test",
                this.strategy.getTag("test", "0.0.0")
        );
    }
}
