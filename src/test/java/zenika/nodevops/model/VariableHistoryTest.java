package zenika.nodevops.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by jules on 13/06/17.
 *
 * Test VariableHistory object
 */
@RunWith(SpringRunner.class)
@JsonTest
public class VariableHistoryTest {

    @Autowired
    private JacksonTester<VariableHistory> json;

    private VariableHistory variableHistory;

    @Before
    public void setUp() {
        this.variableHistory = ImmutableVariableHistory
                .builder()
                .origin("test")
                .value("test")
                .lineNumber(0)
                .build();
    }

    @Test
    public void testBuilder() {
        Assert.assertNotNull(
                "Initialization failed",
                ImmutableVariableHistory.builder()
                        .value("test")
                        .origin("test")
                        .lineNumber(0)
                        .build()
        );
    }

    @Test
    public void testFieldValue() {
        Assert.assertEquals(
                "Field value not initialized!",
                "test",
                this.variableHistory.value()
        );
    }

    @Test
    public void testFieldOrigin() {
        Assert.assertEquals(
                "Field origin not initialized!",
                "test",
                this.variableHistory.origin()
        );
    }

    @Test
    public void testFieldLineNumber() {
        Assert.assertEquals(
                "Field lineNumber not initialized!",
                0,
                this.variableHistory.lineNumber()
        );
    }

    @Test
    public void testImmutable() {
        /*
         * These tests will fail due to Immutability
         */
        // variableHistory.origin = "fail";
        // variableHistory.value = "fail";
    }

    @Test
    public void testSerialize() throws IOException {
        JsonContent<VariableHistory> contentJson = this.json.write(variableHistory);

        assertThat(contentJson).isEqualToJson("VariableHistoryExpected.json");
    }

    @Test
    public void testDeserialize() throws IOException {
        VariableHistory variableHistory = this.json.readObject("VariableHistoryExpected.json");
        Assert.assertEquals(
                "Deserialization failed!",
                "test",
                variableHistory.value()
        );
        Assert.assertEquals(
                "Deserialization failed!",
                "test",
                variableHistory.origin()
        );
    }
}
