package zenika.nodevops.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by jules on 13/06/17.
 *
 * Test Variable object
 */
@RunWith(SpringRunner.class)
@JsonTest
public class VariableTest {

    private final int NBADDING = 3;

    @Autowired
    private JacksonTester<Variable> json;

    private Variable variable;

    @Before
    public void setUp() {
        this.variable = new Variable("test");
    }

    @Test
    public void testBuilder() {
        Assert.assertNotNull(
                "Initialization failed",
                new Variable("test")
        );
    }

    @Test
    public void testGetName() {
        Assert.assertEquals(
                "Initialization name failed!",
                "test",
                this.variable.getName()
        );
    }

    @Test
    public void testGetValue() {
        Assert.assertEquals(
                "Initialization value failed!",
                Variable.UNDEFINED,
                this.variable.getValue()
        );
    }

    @Test
    public void testGetOrigin() {
        Assert.assertEquals(
                "Initialization origin failed!",
                Variable.UNDEFINED,
                this.variable.getOrigin()
        );
    }

    @Test
    public void testGetValuesHistory() {
        Assert.assertEquals(
                "VariableHistory initialized not empty!",
                0,
                this.variable.getValuesHistory().size()
        );
    }

    @Test
    public void testToString() {
        Assert.assertEquals(
                "toString wrong split char!",
                "test" + " = " + Variable.UNDEFINED,
                this.variable.toString()
        );
    }

    @Test
    public void testAddStepHistory() {
        variable.addStepHistory(
                ImmutableVariableHistory
                        .builder()
                        .origin("test")
                        .value("test")
                        .lineNumber(1)
                        .build()
        );
        Assert.assertEquals(
                "Wrong adding history",
                1,
                variable.getValuesHistory().size()
        );
    }

    @Test
    public void testFinalValue() {
        variable.addStepHistory(
                ImmutableVariableHistory
                        .builder()
                        .value("test")
                        .origin("test")
                        .lineNumber(1)
                        .build()
        );
        variable.addStepHistory(
                ImmutableVariableHistory
                        .builder()
                        .value("value")
                        .origin("origin")
                        .lineNumber(1)
                        .build()
        );
        Assert.assertEquals(
                "Initialization failed",
                "value",
                variable.getValue()
        );
    }

    @Test
    public void testFinalOrigin() {
        variable.addStepHistory(
                ImmutableVariableHistory
                        .builder()
                        .value("test")
                        .origin("test")
                        .lineNumber(1)
                        .build()
        );
        variable.addStepHistory(
                ImmutableVariableHistory
                        .builder()
                        .value("value")
                        .origin("origin")
                        .lineNumber(1)
                        .build()
        );
        Assert.assertEquals(
                "Initialization failed",
                "origin",
                variable.getOrigin()
        );
    }

    @Test
    public void testSerialize() throws IOException {
        JsonContent<Variable> contentJson = this.json.write(variable);

        assertThat(contentJson).isEqualToJson("VariableExpected.json");
    }

    @Test
    public void testDeserialize() throws IOException {
        Variable variable = this.json.readObject("VariableExpected.json");
        Assert.assertEquals(
                "Deserialization failed!",
                "test",
                variable.getName()
        );
        Assert.assertEquals(
                "Deserialization failed!",
                Variable.UNDEFINED,
                variable.getValue()
        );
    }
}
