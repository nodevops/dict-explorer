package zenika.nodevops.model;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by jules on 06/07/17.
 */
public class WhereTest {

    @Test
    public void testMessageError() {
        Assert.assertEquals(
                "ERROR message fail!",
                "ERROR",
                Where.ERROR.toString()
        );
    }

    @Test
    public void testMessageBothSame() {
        Assert.assertEquals(
                "BOTH_SAME message fail!",
                "variables_available_on_both_and_same_value",
                Where.BOTH_SAME.toString()
        );
    }

    @Test
    public void testMessageBothDiff() {
        Assert.assertEquals(
                "BOTH_DIFF message fail!",
                "variables_available_on_both_but_different_value",
                Where.BOTH_DIFF.toString()
        );
    }

    @Test
    public void testMessageOnlyLeft() {
        Assert.assertEquals(
                "ONLY_LEFT message fail!",
                "variables_only_available_in_left",
                Where.ONLY_LEFT.toString()
        );
    }

    @Test
    public void testMessageOnlyRight() {
        Assert.assertEquals(
                "ONLY_RIGHT message fail!",
                "variables_only_available_in_right",
                Where.ONLY_RIGHT.toString()
        );
    }
}
